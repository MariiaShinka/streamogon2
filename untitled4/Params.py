import os
API = {'alpha': 'https://api.mystagetv-alpha-c7.my.cloud.devmail.ru/', 'rc': 'https://api.mystagetv-rc-c7.my.cloud.devmail.ru/', 'prod': 'https://api.mystage.tv/'}
CONTROL = {'alpha': 'https://control.mystagetv-alpha-c7.my.cloud.devmail.ru/', 'rc': 'https://control.mystagetv-rc-c7.my.cloud.devmail.ru/', 'prod': 'https://control.mystage.tv/'}
STREAM_URL = {'alpha': 'rtmp://stream1.my.cloud.devmail.ru/stream', 'rc': 'rtmp://stream4.my.cloud.devmail.ru/stream', 'prod': 'rtmp://ingest2.mystage.tv/stream'}

TASK_ID_LIST = list()
STREAM_ID_LIST = list()
USER_ID_LIST = list()

def check_task_id_list():
	if not (TASK_ID_LIST.__len__()<5):
		os.kill(TASK_ID_LIST[0], 1)
		TASK_ID_LIST.remove(TASK_ID_LIST[0])
		STREAM_ID_LIST.remove(STREAM_ID_LIST[0])
		USER_ID_LIST.remove(USER_ID_LIST[0])
		print("--------------- TASK_ID_LIST".format(TASK_ID_LIST))
		print("--------------- STREAM_ID_LIST".format(STREAM_ID_LIST))
		print("--------------- USER_ID_LIST".format(USER_ID_LIST))


def kill_stream_by_user_id(environment, user_id):
	user_id_and_environment = user_id+environment
	stream_index = USER_ID_LIST.index(user_id_and_environment)
	os.kill(TASK_ID_LIST[stream_index], 1)
	TASK_ID_LIST.remove(TASK_ID_LIST[stream_index])
	STREAM_ID_LIST.remove(STREAM_ID_LIST[stream_index])
	USER_ID_LIST.remove(USER_ID_LIST[stream_index])
	print("--------------- TASK_ID_LIST".format(TASK_ID_LIST))
	print("--------------- STREAM_ID_LIST".format(STREAM_ID_LIST))
	print("--------------- USER_ID_LIST".format(USER_ID_LIST))


def Stream_run_update(task_id, stream_id, user_id, environment):
	TASK_ID_LIST.append(task_id)
	STREAM_ID_LIST.append(stream_id)
	USER_ID_LIST.append(user_id+environment)
	print("TASK_ID_LIST is {}".format(TASK_ID_LIST))
	print("STREAM_ID_LIST is {}".format(STREAM_ID_LIST))
	print("USER_ID_LIST is {}".format(USER_ID_LIST))
