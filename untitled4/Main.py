from flask import Flask
from flask import render_template
from .All_methods import *
from flask import request
app = Flask(__name__, static_url_path='', static_folder='static')

@app.route("/")
def index():
	return render_template('index.html')

@app.route("/start_stream", methods=['POST'])
def start_stream():
	user_id = request.form['user_id']
	environment = request.form['env_select']
	make_stream_url_with_user_id(environment, user_id)


@app.route("/start_stream_rtmp_key", methods=['POST'])
def start_stream_rtmp_key():
	rtmp_url = request.form['rtmp']
	stream_key = request.form['stream_key']
	make_stream_with_url_and_key_general(rtmp_url, stream_key)
	return "Done"




@app.route('/stop_all_streams', methods=['post'])
def stop_all_streams():
	print("------------TASK_ID_LIST is {}".format(TASK_ID_LIST))
	for item in TASK_ID_LIST:
		print("------------TASK_ID_LIST {}".format(item))
		os.kill(item, 1)
		TASK_ID_LIST.remove(TASK_ID_LIST[0])
		STREAM_ID_LIST.remove(STREAM_ID_LIST[0])
		USER_ID_LIST.remove(USER_ID_LIST[0])


@app.route('/start_stream/<environment>/<user_id>', methods=['post'])
def start_stream_by_id(environment, user_id):
	make_stream_url_with_user_id(environment, user_id)


@app.route("/stop_stream/<environment>/<user_id>", methods=['POST'])
def stop_stream_by_id(environment, user_id):
	kill_stream_by_user_id(environment, user_id)


if __name__ == '__main__':
	app.run(debug=True)