from requests import get
from requests import post
from random import choice
from .Params import *
import json
import subprocess


def get_authorization_token(environment, user_id):
	response = get(CONTROL[environment]+'x/user/'+user_id, headers={'X-Auth-User': '1', 'X-Auth-User-Digest': ''}, verify=False).content.decode()
	json_result = json.loads(response)
	access_tokens = json_result['access_tokens']
	is_streamer = json_result['is_streamer']
	token = choice(access_tokens)
	print("----------current token is "+token)
	return {'Authorization': 'Bearer {}'.format(token)}


def get_current_stream_info(environment, user_id):
	print('http get {}user/{}/stream'.format((API[environment]), user_id))
	response_main = get(API[environment]+"user/"+user_id+"/stream", verify=False)
	response = response_main.content.decode()
	print('http get {}'.format(response_main.url))
	json_result = json.loads(response)
	stream_not_found = response.__contains__('stream_not_found')
	print(json_result)
	if stream_not_found:
		print("----------stream_not_found")
		return
	else:
		json_result = json.loads(response)
		current_stream_id_result = json_result['id']
		print('----------stream_id {}'.format(current_stream_id_result))
		return current_stream_id_result


def get_stream_key(environment, user_id):
	response = get(CONTROL[environment]+'x/user/'+user_id, headers={'X-Auth-User': '1', 'X-Auth-User-Digest': ''}, verify=False).content.decode()
	json_result = json.loads(response)
	stream_key = json_result['stream_key']
	print("----------current stream_key {}".format(stream_key))
	return str(stream_key)
	
	
def post_stream(api_environment, control_environment, user_id):
	token = get_authorization_token(control_environment, user_id)
	response = post(api_environment  + "stream", headers=token, verify=False)
	print("http POST " +response.url)
	print("-------status_code  {}".format(response.status_code))
	new_stream_id = json.loads(response.content.decode())['id']
	return new_stream_id


def make_stream_url_with_user_id(environment, user_id):
	stream_url = STREAM_URL[environment]
	stream_key = get_stream_key(environment, user_id)
	make_stream_with_url_and_key(stream_url, stream_key, environment, user_id)
 

def make_stream_with_url_and_key(rtmp_url, stream_key, environment, user_id):
	check_task_id_list()
	task_id = make_stream_with_url_and_key_general(rtmp_url, stream_key)
	stream_id = get_current_stream_info(environment, user_id)
	Stream_run_update(task_id, stream_id, user_id, environment)


def make_stream_with_url_and_key_general(rtmp_url, stream_key):
	stream_url_with_key = rtmp_url+'/'+stream_key
	p = subprocess.Popen(
		'ffmpeg -re -i Aliexpress.mp4  -c copy -f flv ' + stream_url_with_key,
		stdout=subprocess.PIPE, shell=True)
	return p.pid
